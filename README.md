# README #

Example CRUD Server for a TODO-List.

Based on webx 3.3.1 (Develop branch)
https://bitbucket.org/moritzthielcke/webx.io/commits/branch/develop



### Building the Project ###

* Build the project with mvn clean install.
* Navigate to target/deploy/
* Execute the jar file:
todoserver/target/deploy$ java -jar todoserver.jar

2015-12-03 14:20:29,658 INFO  [main] cache.EhCacheImpl (EhCacheImpl.java:26): EhCache init

....lots of output :)

2015-12-03 14:20:43,002 INFO  [main] http.Core (Core.java:84): > starting 3 HTTP server on http://0.0.0.0:8080



### Creating a todo ###

* HTTP POST http://127.0.0.1:8080/todo

Body:
```
#!javascript 
{"title" : "my first todo", "details" : "some details about my todo" }

```

* Example Response

Http Status Code: 201

Http Header : 'Location' : '/todo/9ce9c90a-0765-45b8-b2c3-fb4d315568c0' 


### Reading all todos ###

* HTTP GET http://127.0.0.1:8080/todo

* Example Response

Http Status Code: 200

Http Body: [{"createdDate":1449148986859,"due":null,"id":"9ce9c90a-0765-45b8-b2c3-fb4d315568c0","title":"my first todo","details":"some details about my todo"},{"createdDate":1449149186348,"due":null,"id":"fc31d9e4-f420-431d-852b-8749668ecbd5","title":"my first todo","details":"some details about my todo"}]


### Reading a single todo ###
* HTTP GET http://localhost:8080/todo/9ce9c90a-0765-45b8-b2c3-fb4d315568c0 (http://localhost:8080/todo/{id})

* Example Response

Http Status Code: 200

Http Body: {"createdDate":1449149575714,"due":null,"id":"9ce9c90a-0765-45b8-b2c3-fb4d315568c0","title":"my first todo","details":"some details about my todo"}



### Updating a todo ###

* HTTP PUT http://localhost:8080/todo/d84a28e8-9747-4106-84b3-e1d0029349f6 (http://localhost:8080/todo/{id})

Http Body: {"createdDate":1449149575714,"due":null,"id":"9ce9c90a-0765-45b8-b2c3-fb4d315568c0","title":"my first todo","details":"some details about my todo"}

* Example Response

Http Status Code: 201

Http Header : 'Location' : '/todo/9ce9c90a-0765-45b8-b2c3-fb4d315568c0' 


### Deleting a todo ###

* HTTP DELETE http://localhost:8080/todo/9ce9c90a-0765-45b8-b2c3-fb4d315568c0 (http://localhost:8080/todo/{ID})

* Example Response

Http Status Code: 204


### Service Information ###
http://localhost:8080/about
http://localhost:8080/about/ram