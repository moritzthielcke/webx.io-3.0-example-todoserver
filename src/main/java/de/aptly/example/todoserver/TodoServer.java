/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.example.todoserver;

import io.wx.core3.common.controller.ApplicationDetailController;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.CloudConfigurator;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.app.AbstractApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class TodoServer extends AbstractApplication{
     private static final Logger logger = LogManager.getLogger(TodoServer.class);    
    
 
    
    public static void main(String[] args) throws Exception {
        AppConfig cfg = new SimpleConfig().setInstanceCount(3).setPort(8080);
        cfg.getModuleController().put("", ApplicationDetailController.class);
        TodoServer server = new TodoServer(cfg);
        server.start();
        logger.info("> Stage = "+server.getConfig().getStage());
        server.idle();
    }
    
    
    
    public TodoServer(AppConfig cfg) throws Exception{
        super(cfg, new CloudConfigurator());
    }
    
    
    @Override
    public String getApplicationPath() {
       return "de.aptly.example.todoserver.app";
    }
    
    
    
}
