/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.example.todoserver.app.ctr;

import de.aptly.example.todoserver.TodoServer;
import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/")
public class HelloWorldController {
    private static final Logger logger = LogManager.getLogger(TodoServer.class); 
    @Inject RequestController ctr;     
    
    @RequestMapping(method = HttpMethod.GET)    
    public String getTestData() {
        logger.info("heeeyz world!");
        return "hello world"; 
    }    
}
