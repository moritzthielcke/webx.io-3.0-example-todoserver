/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.example.todoserver.app.ctr;

import de.aptly.example.todoserver.app.exceptions.NotFoundException;
import de.aptly.example.todoserver.beans.Todo;
import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.HttpStatus;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.http.traits.TraitConf;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.inject.Inject;

/**
 *
 * @author moritz
 */
@Resource(path = "/todo", traits = {@TraitConf(trait = BodyHandlerTrait.class)})
public class TodoController {
    private static final Map<String, Todo> todoList = new HashMap<>();
    @Inject RequestController ctr;     
    
    
    @RequestMapping(method = HttpMethod.GET)    
    public Collection<Todo> getTestData() {
        return todoList.values(); 
    }
    
    @RequestMapping(method = HttpMethod.GET, path = "/:id")
    public Todo getTodoById(@URLParam String id){
        Todo todo = todoList.get(id);
        if(todo == null){
            throw new NotFoundException();
        }
        return todo;
    }
    
    @RequestMapping(method = HttpMethod.POST, httpStatus = HttpStatus.CREATED)        
    public Todo postTodo(Todo todo){
        todo.setId(UUID.randomUUID().toString());
        todo.setCreatedDate(new Date());
        todoList.put(todo.getId(), todo);
        return todo;
    }
    
    @RequestMapping(method = HttpMethod.PUT, path = "/:id", httpStatus = HttpStatus.CREATED)   
    public Todo putTodo(@URLParam String id, Todo todo){ 
       todo.setId(id);
       Todo old = todoList.get(id);
       if(old != null){
           todo.setCreatedDate(old.getCreatedDate());
       } 
       else{
           todo.setCreatedDate(new Date());
       }
       todoList.put(id, todo);
       return todo;
    }

    @RequestMapping(method = HttpMethod.DELETE, path = "/:id", httpStatus = HttpStatus.NO_CONTENT)   
    public void deleteTodo(@URLParam String id){ 
       Todo todo = todoList.remove(id);
       if(todo == null){
           throw new NotFoundException();
       }
       ctr.next();
    }   
}